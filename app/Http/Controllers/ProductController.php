<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    //S
    function store(Request $request)
    {
        $product = Product::create([
            'name' => $request->name,
            'file_path' => $request->file_path,
            'description' => $request->description,
            'price' => $request->price,
        ]);
        if ($request->hasFile('file_path')) {
            $request->file('file_path')->move('photoproduct/', $request->file('file_path')->getClientoriginalName());
            $product->file_path = $request->file('file_path')->getClientoriginalName();
            $product->save();
        }

        return response()->json(['massage' => 'berhasil tambah product']);

    }

    function list()
    {
        return Product::all();
    }
    function delete($id)
    {
        $result = Product::where('id', $id)->delete();
        if ($result) {
            return ["result" => "Product has been deleted"];
        } else {
            return ["result" => "Operation failed"];
        }
    }

    function getProduct($id)
    {
        return Product::find($id);
    }

    function updateproduct($id, Request $request)
    {
        //return $req->input();
        $product = Product::findOrFail($id);
        $product->update($request->all());

        return response()->json(['message' => 'Data Berhasil di Update']);

    }

    

    function search($key)
    {
        return Product::where('name', 'like', "%$key%")->get();
    }
}